OpenGL tests (Linux and Windows)
============

What is this ?
--------------

This repository contains C++ code relative to OpenGL.
Use this code at your own risks.

Where does it work
-------------

Currently, all build and runtime issues are fixed on Linux. Should work on Windows too (thanks in advance for any feedback)


Get OpenGL version
------------------

With glxinfo:
```
glxinfo -B
```
( -B option returns the most important information about what you can expect from your graphic card)

With glewinfo
```
glewinfo | grep OpenGL
```

Resources
---------

- Modern OpenGL introduction [Wikibooks](https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Introduction)
- LazyFoo SDL2/OpenGL [introduction](http://lazyfoo.net/tutorials/SDL/50_SDL_and_opengl_2/index.php)

Content details
---------------

- 001 : Use OpenGL / GLSL with SDL2
- 001_comp : Complementary exercises based upon 001
- 002 : Externalize shaders into glsl files, access them by using SDL_GetBasePath, debug added for shaders
- 003 : Using sinus with SDL_GetTicks to vary alpha of the rendered triangle through uniform value
- 004 : Coordinates switch into 3D and rotation using glm lib.


