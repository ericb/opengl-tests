#include <iostream>

#include "ogl_window.h"
#undef main

using namespace std;

int main()
{
	OGLWindow lWindow;
	if(lWindow.run() == false)
	{
		cerr << "An error has occured while loading opengl window !" << endl;
		cerr << "Please take a look on the previous messages." << endl;
	}

	return 0;
}
