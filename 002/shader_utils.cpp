#include "shader_utils.h"

#include <SDL2/SDL.h>
#include <iostream>
#include <fstream>

using namespace std;

static std::string shader_src;

//Load a file and return its content
const char* load(string pFilePath)
{
    // inspired from https://gamedev.stackexchange.com/questions/105605/glsl-shaders-work-when-hardcoded-not-when-loaded-from-file?rq=1
    ifstream shader_file(pFilePath, std::ios::in | std::ios::binary);

    if(shader_file.is_open())
    {
        shader_file.seekg(0, ios::end);
        shader_src.resize(shader_file.tellg());

        shader_file.seekg(0, ios::beg);
        shader_file.read(&shader_src[0], shader_src.size());

        shader_file.close();
        return shader_src.c_str();
    }
    else
        return nullptr;
}

//Use to debug a shader or a program 
void debug(GLuint gl_object)
{
    GLint log_length = 0;

    if(glIsShader(gl_object))
        glGetShaderiv(gl_object, GL_INFO_LOG_LENGTH, &log_length);
    else if(glIsProgram(gl_object))
        glGetProgramiv(gl_object, GL_INFO_LOG_LENGTH, &log_length);

    char* log = (char*)malloc(log_length);

    if(glIsShader(gl_object))
        glGetShaderInfoLog(gl_object, log_length, nullptr, log);
    else if(glIsProgram(gl_object))
        glGetProgramInfoLog(gl_object, log_length, nullptr, log);

    cerr << log << endl;
    free(log);
}

//Create the shader of the given type from a file
GLuint create_shader(std::string pFilePath, GLenum type)
{
    GLint compile_status;
    const char* shader_src = load(pFilePath);
#ifdef DEBUG
    std::cout  << "pfilePath  = " << pFilePath.c_str() << std::endl;
#endif
    if(shader_src == nullptr)
    {
        cerr << "Cannot load shader (probably due to incorrect path) : " << pFilePath << endl;
        return 0;
    }

    GLuint res = glCreateShader(type);

    glShaderSource(res, 1, &shader_src, nullptr);
    glCompileShader(res);
    glGetShaderiv(res, GL_COMPILE_STATUS, &compile_status);

    if(compile_status == GL_FALSE)
    {
        cerr << "Cannot compile shader from : " << pFilePath << endl;
        debug(res);
        glDeleteShader(res);
        return 0;
    }
#ifdef DEBUG
    else
        std::cerr << "Vertex shader compiled" << std::endl;
#endif

    return res;
}

