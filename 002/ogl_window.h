#ifndef OGL_WINDOW_H
#define OGL_WINDOW_H

#include <string>

//Must be loaded before SDL_opengl.h
#include <GL/glew.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

class OGLWindow
{
    private:
        bool is_running;  
        SDL_Window* display;
        SDL_GLContext context;
        GLint attribute_coord2d;
        std::string base_path;
        GLuint vbo_triangle;

    public:
        GLuint program;
        //Constructor
        OGLWindow()
        {
            display = nullptr;
        }

        //Initialize the game display
        bool init();

        //Create GLSL resources
        bool init_resources(void);

        //Display the game
        bool run();

        //Catch game events
        void on_event(SDL_Event* event);
};

#endif
