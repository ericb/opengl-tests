#include "ogl_window.h"
#include "shader_utils.h"
#include <iostream>

#ifdef Linux
#ifndef GL_GLEXT_PROTOTYPES
#define GL_GLEXT_PROTOTYPES
#endif
#ifndef GL3_PROTOTYPES
#define GL3_PROTOTYPES 1
#endif
#endif

#define VERTEX_SHADER    "assets/shaders/vertex_shader.glsl"
#define FRAGMENT_SHADER   "assets/shaders/fragment_shader.glsl"

const char* attribute_name = "coord2d";


//Initialize an SDL window in fullscreen mode
bool OGLWindow::init()
{
    //Try to initailize all SDL component and check if it works
    if(SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO|SDL_INIT_EVENTS) < 0)
        return false;

    //Setup the OpenGL version (2.1)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    //Create a window and check if it works
    display = SDL_CreateWindow("Test OpenGL 001",
                                SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                640, 480, SDL_WINDOW_OPENGL);
    if(display == nullptr)
    {
        std::cerr << "Cannot initialize opengl window" << std::endl;
        return false;
    }

    //Initialize OpenGL context
    context = SDL_GL_CreateContext(display);
    if(context == nullptr)
    {
        std::cerr << "Cannot initialize the OpenGL context" << std::endl;
        return false;
    }

    //Initialize GLEW
#ifdef Linux
    glewExperimental = GL_TRUE;
#endif
    GLenum glew_status = glewInit();

    if(glew_status != GLEW_OK)
    {
        std::cerr << "GLEW error:" << glewGetErrorString(glew_status) << std::endl;
        return false;
    }

    //First initialize base path, then init_resources and return
    char* path = SDL_GetBasePath();

    if(path == nullptr)
        this->base_path = "./";
    else
    {
        base_path = SDL_strdup(path);
        SDL_free(path);
    }


    if(!init_resources())
        return false;

    return true;
}



//Create GLSL resources
bool OGLWindow::init_resources(void)
{
    GLint link_status;

    //Define triangle in vertex buffer object (vbo)
    GLfloat triangle_vertices[] = {
        0.0, 0.8,
        -0.8, -0.8,
        0.8, -0.8
    };
    glGenBuffers(1, &vbo_triangle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_vertices), triangle_vertices, GL_STATIC_DRAW);

    //Define the vertex shader
    std::string vs_path = this->base_path + VERTEX_SHADER;

    GLuint vertex_shader = create_shader(vs_path, GL_VERTEX_SHADER);

    if(vertex_shader == 0)
    {
        std::cerr << "Cannot compile the vertex shader" << std::endl;
        return false;
    }

    //Define the fragment shader
    std::string fs_path = this->base_path + FRAGMENT_SHADER;

    GLuint fragment_shader = create_shader(fs_path, GL_FRAGMENT_SHADER);

    if(fragment_shader == 0)
    {
        std::cerr << "Cannot compile the fragment shader" << std::endl;
        return false;
    }

    //Combine the vertex and fragment shaders to make a GLSL program
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &link_status);
    if(link_status == GL_FALSE)
    {
        std::cerr << "Cannot link GLSL program !" << std::endl;
        debug(program);
        return false;
    }

    attribute_coord2d = glGetAttribLocation(program, attribute_name);

    if(attribute_coord2d == -1)
    {
        std::cerr << "Invalid attribute " << attribute_name << std::endl;
        return false;
    }

    return true;
}

bool OGLWindow::run()
{
    is_running = init();

    if(is_running == false)
    {
        std::cerr << "SDL initialization phase fails !" << std::endl;
        return false;
    }

    SDL_Event lEvent;

    while(is_running)
    {
        if(SDL_PollEvent(&lEvent))
        {
            on_event(&lEvent);
        }

        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);
        glEnableVertexAttribArray(attribute_coord2d);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle);

        glVertexAttribPointer(
            attribute_coord2d,
            2, //X and Y
            GL_FLOAT, //type
            GL_FALSE,
            0, //no extra data between each pos
            0
        );

        glDrawArrays(GL_TRIANGLES, 0, 3);
        glDisableVertexAttribArray(attribute_coord2d);

        SDL_GL_SwapWindow(display);
    }

    glDeleteProgram(program);
    glDeleteBuffers(1, &vbo_triangle);
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(display);
    SDL_Quit();
    return true;
}

//Handle SDL events 
void OGLWindow::on_event(SDL_Event* pEvent)
{
    switch(pEvent->type)
    {
        case SDL_QUIT:
        is_running = false;
        break;

        default:
        break;
    }
}


