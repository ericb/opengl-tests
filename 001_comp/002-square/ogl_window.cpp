#include "ogl_window.h"
#include <iostream>

//Initialize an 720p SDL window in fullscreen mode
bool OGLWindow::init()
{
	//Try to initailize all SDL component and check if it works
	if(SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO|SDL_INIT_EVENTS) < 0)
	{
		return false;
	}

	//Setup the OpenGL version (2.1)
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	//Create a window and check if it works
	display = SDL_CreateWindow("Test OpenGL 001",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		       	640, 480, SDL_WINDOW_OPENGL);
	
	if(display == nullptr)
	{
		std::cerr << "Cannot initialize opengl window" << std::endl;
		return false;
	}

	//Initialize OpenGL context
	context = SDL_GL_CreateContext(display);
	if(context == nullptr)
	{
		std::cerr << "Cannot initialize the OpenGL context" << std::endl;
		return false;
	}

	//Initialize GLEW
	GLenum glew_status = glewInit();
	if(glew_status != GLEW_OK)
	{
		std::cerr << "GLEW error:" << glewGetErrorString(glew_status) << std::endl;
		return false;
	}

	if(!init_resources())
	{
		return false;
	}

	//Initialize base path (just in case)
	char* path = SDL_GetBasePath();
	if(path == nullptr)
	{
		base_path = "./";
	}
	else
	{
		base_path = SDL_strdup(path);
		SDL_free(path);
	}
	
	return true;
}

//Create GLSL resources
bool OGLWindow::init_resources(void)
{
	GLint compile_status;
	GLint link_status;

	//Define and compile the vertex shader (65 to 86)
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);

	//Horrible way to load the shader but it just for test
	const char* vertex_shader_src = 
		#ifdef GL_ES_VERSION_2_0
			"#version 100\n" //GLES 2.0
		#else
			"#version 120\n" //GLES 2.1
		#endif
		"attribute vec2 coord2d;"
		"void main(void){"
			"gl_Position = vec4(coord2d, 0.0, 1.0);"
		"}";

	glShaderSource(vertex_shader, 1, &vertex_shader_src, nullptr);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);
	if(compile_status == GL_FALSE)
	{
		std::cerr << "Cannot compile the vertex shader" << std::endl;
		return false;
	}

	//Define the fragment shader
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

	//Again this horrible method	
	const char* fragment_shader_src = 
		#ifdef GL_ES_VERSION_2_0
			"#version 100\n" //GLES 2.0
		#else
			"#version 120\n" //GLES 2.1
		#endif
		"void main(void){"
			"gl_FragColor[0] = 0.0;" //R
			"gl_FragColor[1] = 0.0;" //G
			"gl_FragColor[2] = 1.0;" //B
		"}";
	
	glShaderSource(fragment_shader, 1, &fragment_shader_src, nullptr);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);
	if(compile_status == GL_FALSE)
	{
		std::cerr << "Cannot compile the fragment shader" << std::endl;
		return false;
	}

	//Combine the vertex and fragment shaders to make a GLSL program
	program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &link_status);
	if(link_status == GL_FALSE)
	{
		std::cerr << "Cannot link GLSL program !" << std::endl;
		return false;
	}

	const char* attribute_name = "coord2d";
	attribute_coord2d = glGetAttribLocation(program, attribute_name);
	if(attribute_coord2d == -1)
	{
		std::cerr << "Invalid attribute " << attribute_name << std::endl;
		return false;
	}

	return true;
}

bool OGLWindow::run()
{
	is_running = init();
	if(is_running == false)
	{
		std::cerr << "SDL initialization phase fails !" << std::endl;
		return false;
	}

	SDL_Event lEvent;

	while(is_running)
	{
		if(SDL_PollEvent(&lEvent))
		{
			on_event(&lEvent);
		}

		glClearColor(1.0, 1.0, 1.0, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(program);
		glEnableVertexAttribArray(attribute_coord2d);

		GLfloat triangle1_vertices[] = {
			-0.5, 0.5,
			-0.5, -0.5,
			0.5, 0.5
		};

		GLfloat triangle2_vertices[] = {
			0.5, 0.5,
			0.5, -0.5,
			-0.5, -0.5
		};

		glVertexAttribPointer(
			attribute_coord2d,
			2, //X and Y
			GL_FLOAT, //type
			GL_FALSE,
			0, //no extra data between each pos
			triangle1_vertices //previously defined points
		);

		glDrawArrays(GL_TRIANGLES, 0, 3);

		glVertexAttribPointer(
			attribute_coord2d,
			2, //X and Y
			GL_FLOAT, //type
			GL_FALSE,
			0, //no extra data between each pos
			triangle2_vertices //previously defined points
		);

		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDisableVertexAttribArray(attribute_coord2d);

		SDL_GL_SwapWindow(display);
	}
		
	glDeleteProgram(program);
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(display);
	SDL_Quit();
	return true;
}

//Handle SDL events 
void OGLWindow::on_event(SDL_Event* pEvent)
{
	switch(pEvent->type)
	{
		case SDL_QUIT:
			is_running = false;
			break;
	}
}	

