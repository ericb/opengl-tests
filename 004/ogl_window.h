#ifndef OGL_WINDOW_H
#define OGL_WINDOW_H

#include <string>

//Must be loaded before SDL_opengl.h
#include <GL/glew.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

class OGLWindow
{
    private:
        bool is_running;  
        std::string base_path;

        SDL_Window* display;
        SDL_GLContext context;

        struct attributes {
            GLfloat coord3d[3];
            GLfloat v_color[3];
        };

        GLuint vbo_triangle;
        GLuint program;
        GLint attribute_coord3d, attribute_colors;
        GLint uniform_m_transform;

        float current_alpha;
        float current_angle;
        GLint uniform_alpha;

    public:
        //Constructor
        OGLWindow()
        {
            display = nullptr;
        }

        //Initialize the display
        bool init();

        //Create GLSL resources
        bool init_resources(void);

        //Display the game
        bool run();

        //Handle events
        void on_event(SDL_Event* event);
};

#endif
