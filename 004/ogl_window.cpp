#include "ogl_window.h"
#include "shader_utils.h"

#include <iostream>
#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//Initialize an SDL window in fullscreen mode
bool OGLWindow::init()
{
    //Try to initailize all SDL component and check if it works
    if(SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO|SDL_INIT_EVENTS) < 0)
        return false;

    //Setup the OpenGL version (2.1)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    //Create a window and check if it works
    display = SDL_CreateWindow("Test OpenGL 004",
                                SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                640, 480, SDL_WINDOW_OPENGL);
    if(display == nullptr)
    {
        std::cerr << "Cannot initialize opengl window" << std::endl;
        return false;
    }

    //Initialize OpenGL context
    context = SDL_GL_CreateContext(display);
    if(context == nullptr)
    {
        std::cerr << "Cannot initialize the OpenGL context" << std::endl;
        return false;
    }

    //Initialize GLEW
    GLenum glew_status = glewInit();
    if(glew_status != GLEW_OK)
    {
        std::cerr << "GLEW error:" << glewGetErrorString(glew_status) << std::endl;
        return false;
    }

    if(!init_resources())
        return false;

    // Enable alpha
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Initialize base path
    char* path = SDL_GetBasePath();

    if(path == nullptr)
    {
        base_path = "./";
    }
    else
    {
        base_path = SDL_strdup(path);
        SDL_free(path);
    }

    return true;
}

//Create GLSL resources
bool OGLWindow::init_resources(void)
{
    GLint link_status;

    //Define triangle attributes (vertices + colors)
    struct attributes triangle_attributes[] = {
        {{0.0, 0.8, 0.0}, {1.0, 0.0, 0.0}},
        {{-0.8, -0.8, 0.0}, {0.0, 1.0, 0.0}},
        {{0.8, -0.8, 0.0}, {0.0, 0.0, 1.0}} 
    };

    glGenBuffers(1, &vbo_triangle);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_attributes), triangle_attributes, GL_STATIC_DRAW);

    //Define the vertex shader
    std::string vs_path = base_path + "assets/shaders/vertex_shader.glsl";
    GLuint vertex_shader = create_shader(vs_path, GL_VERTEX_SHADER);

    if(vertex_shader == 0)
    {
        std::cerr << "Cannot compile the vertex shader" << std::endl;
        return false;
    }

    //Define the fragment shader
    std::string fs_path = base_path + "assets/shaders/fragment_shader.glsl";
    GLuint fragment_shader = create_shader(fs_path, GL_FRAGMENT_SHADER);
    if(fragment_shader == 0)
    {
        std::cerr << "Cannot compile the fragment shader" << std::endl;
        return false;
    }

    //Combine the vertex and fragment shaders to make a GLSL program
    program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &link_status);

    if(link_status == GL_FALSE)
    {
        std::cerr << "Cannot link GLSL program !" << std::endl;
        debug(program);
        return false;
    }

    std::string attribute_name = "coord3d";
    attribute_coord3d = glGetAttribLocation(program, attribute_name.c_str());

    if(attribute_coord3d == -1)
    {
        std::cerr << "Invalid attribute " << attribute_name << std::endl;
        return false;
    }

    attribute_name = "v_color";
    attribute_colors = glGetAttribLocation(program, attribute_name.c_str());

    if(attribute_colors == -1)
    {
        std::cerr << "Invalid attribute " << attribute_name << std::endl;
        return false;
    }

    std::string uniform_name = "v_alpha";
    uniform_alpha = glGetUniformLocation(program, uniform_name.c_str());

    if(uniform_alpha == -1)
    {
        std::cerr << "Invalid uniform " << uniform_name << std::endl;
        return false;
    }

    uniform_name = "m_transform";
    uniform_m_transform = glGetUniformLocation(program, uniform_name.c_str());

    if(uniform_m_transform == -1)
    {
        std::cerr << "Invalid uniform " << uniform_name << std::endl;
        return false;
    }

    return true;
}

bool OGLWindow::run()
{
    is_running = init();

    if(is_running == false)
    {
        std::cerr << "SDL initialization phase fails !" << std::endl;
        return false;
    }

    SDL_Event lEvent;

    while(is_running)
    {
        if(SDL_PollEvent(&lEvent))
        {
            on_event(&lEvent);
        }

        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(program);

        //Triangle transparency setup
        current_alpha = sin(SDL_GetTicks()/1000.0 * (2*3.1415) / 5) / 2 + 0.5;
        glUniform1f(uniform_alpha, current_alpha);

        //Triangle ratation setup
        current_angle = SDL_GetTicks()/1000.0 * 2; // 2° per second
        glm::vec3 axis_z(0, 0, 1);
        glm::mat4 m_transform = glm::rotate(glm::mat4(1.0f), current_angle, axis_z);
        glUniformMatrix4fv(uniform_m_transform, 1, GL_FALSE, glm::value_ptr(m_transform));

        //Triangle setup
        glEnableVertexAttribArray(attribute_colors);
        glEnableVertexAttribArray(attribute_coord3d);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_triangle);

        glVertexAttribPointer(
            attribute_coord3d,
            3, //X Y Z
            GL_FLOAT, //type
            GL_FALSE,
            sizeof(struct attributes),
            0
        );

        glVertexAttribPointer(
            attribute_colors,
            3,
            GL_FLOAT,
            GL_FALSE,
            sizeof(struct attributes),
            (GLvoid*) offsetof(struct attributes, v_color) //offset (ignore 2 first elements because it's X,Y)
        );

        glDrawArrays(GL_TRIANGLES, 0, 3);
        glDisableVertexAttribArray(attribute_colors);
        glDisableVertexAttribArray(attribute_coord3d);

        SDL_GL_SwapWindow(display);

        //Slow down to 60 FPS
        SDL_Delay(16);
    }

    glDeleteProgram(program);
    glDeleteBuffers(1, &vbo_triangle);
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(display);
    SDL_Quit();
    return true;
}

//Handle SDL events 
void OGLWindow::on_event(SDL_Event* pEvent)
{
    switch(pEvent->type)
    {
        case SDL_QUIT:
        is_running = false;
        break;
    }
}

